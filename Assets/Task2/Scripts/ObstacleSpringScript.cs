using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class ObstacleSpringScript : MonoBehaviour
{
    //�������, ��� ��� ���������� ���� JointSpring, ������ targerPosition ���������� HingeJoint �������� ����������.
    private JointSpring changeOfPositionSpring;
    void Start()
    {
        changeOfPositionSpring = GetComponent<HingeJoint>().spring;
    }
    private void OnCollisionEnter(Collision collision)
    {
        changeOfPositionSpring.targetPosition = -30;
        GetComponent<HingeJoint>().spring = changeOfPositionSpring;
        //���������, ������ ��� ������ ���� �� ����� ���� �������� �� "GetComponent<HingeJoint>().spring.targetPosition = -30;"
    }
    private void OnCollisionExit(Collision collision)
    {
        changeOfPositionSpring.targetPosition = 0;
        GetComponent<HingeJoint>().spring = changeOfPositionSpring;
    }
}
