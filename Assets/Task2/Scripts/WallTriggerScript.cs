using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallTriggerScript : MonoBehaviour
{
    public GameObject ClosingWall2;
    //��������� � ����� ��� ������������ ���� ���� �����
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Ball"))
        {
            ClosingWall2.SetActive(true);
        }
    }
    // ������������ ����� ��� �������� ���� ���� ����� ������� �� �������, �� ������� �� ������������
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ball"))
        {
            ClosingWall2.SetActive(false);
        }
    }
}
