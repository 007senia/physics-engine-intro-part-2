using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchBallScript : MonoBehaviour
{
    //���������� ����� ����� ����������, ����� ��� �� ��������� ��� ������������ �������������� ��� �� �������
    //������ ��������� � ������������ ����� ��������� ��� ��� ������������� �������� �������
    public GameObject ClosingWall;
    private float startingStringTension;
    private float timer;
    public float startingTimer;
    void Start()
    {
        startingStringTension = GetComponent<SpringJoint>().spring;
        timer = startingTimer;
    }

    void Update()
    {
        LaunchBall();
    }

    void LaunchBall()
    {
        if (timer > 2 )
        {
            timer -= Time.deltaTime;
        }
        else if (timer < 2 && timer > 0)
        {
            ClosingWall.SetActive(false);
            timer -= Time.deltaTime;
            GetComponent<SpringJoint>().spring = startingStringTension;
        }
        else
        {
            GetComponent<SpringJoint>().spring = 5000;
            timer = startingTimer;
            ClosingWall.SetActive(true);
        }
    }
}
