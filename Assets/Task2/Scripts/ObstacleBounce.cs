using System.Collections;
using System.Collections.Generic;
using System.Net;
using Unity.VisualScripting;
using UnityEngine;

public class ObstacleBounce : MonoBehaviour
{
    private Material StartingMaterial;
    private float startingBounceTimer;
    private float bounceTimer;
    public float power;
    public Material CollisionMaterial;
    void Start()
    {
        StartingMaterial = GetComponent<MeshRenderer>().material;
        startingBounceTimer = 0.3f;
        bounceTimer = 0;
    }
    void Update()
    {
        ColorChangeBack();
    }
    private void OnCollisionEnter(Collision collision)
    {
        Vector3 direction = collision.gameObject.transform.position - transform.position;
        collision.gameObject.GetComponent<Rigidbody>().AddForce(direction.normalized * power);

        ColorChange();
    }
    private void ColorChange()
    {
        GetComponent<MeshRenderer>().material = CollisionMaterial;
        bounceTimer = startingBounceTimer;
    }
    private void ColorChangeBack()
    {
        if (bounceTimer > 0)
        {
            bounceTimer -= Time.deltaTime;
        }
        else
        {
            GetComponent<MeshRenderer>().material = StartingMaterial;
        }
    }
}
